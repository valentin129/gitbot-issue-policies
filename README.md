# Minds Gitbot Issue Policies

This repository is resonsible for automating and maintaining our issues and merge request. It uses the [Gitlab Triage tool](https://gitlab.com/gitlab-org/gitlab-triage).

## Creating / Modifying

[.triage-policies.yml](.triage-policies.yml)

## Deploying

### Create the namespace

`kubectl create namespace gitbot`

### Creating the secret

- Create a personal access token at https://gitlab.com/-/profile/personal_access_tokens with the `api` scope and **copy it to your clipboard**.
- Save this secret `kubectl -n gitbot create secret generic gitbot-secrets --from-literal=access_token=$(pbpaste)`

### Deploying the cronjob

- `kubectl -n gitbot apply -f k8s-cronjob.yaml`