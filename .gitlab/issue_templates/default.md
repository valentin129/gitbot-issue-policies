<!---
Please read this!

The Definition of Ready (DoR) can be found at https://developers.minds.com/docs/handbook/how-we-work/#definition-of-ready-dor

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "Type::Feature" label and verify the feature you're about to submit isn't a duplicate.
--->

## Goal

<!---
Clearly outline the goal of the issue and why we are doing it
--->


## What needs to be done

<!---
Clearly detail what needs to be done to achieve the goal set out above
--->


## QA

<!---
Consideration is given to how the issue will be demoed and tested prior to being deployed
--->


## UX/Design

<!---
Sufficient wireframes have been presented and understood by the development team. Full mockups are not required and re-usable components are encouraged.
--->

## Mobile

<!---
Does a mobile card need to be created? Any notes for mobile?
--->

- [ ] Is Mobile Required?

## Personas

<!---
Who does this user impact and why (optional)
--->


## Experiments

<!---
Experiments are not required, but it should be made clear if one is expected. If experiments are required, the hypothesis should be outlined.
--->


## Acceptance Criteria

<!--
Everyone should be able to understand what is expected to be delivered
--->

- [ ] ...

## Definition of Ready Checklist

- [ ] Definition Of Done (DoD)
- [ ] Acceptance criteria
- [ ] Weighted
- [ ] QA
- [ ] UX/Design
- [ ] Personas
- [ ] Experiments

/label ~ "Type::Feature"
